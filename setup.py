import setuptools

setuptools.setup(
    name="nagios2statusIO",
    python_requires=">=3.9",
    url="https://gitlab.ics.muni.cz/perun-proxy-aai/python/checkmk-statusio-synchronizer.git",
    description="Flask app for translating Nagios/CheckMK status into Status.IO hosted status page",
    include_package_data=True,
    packages=setuptools.find_packages(),
    install_requires=[
        "setuptools",
        "certifi==2023.7.22",
        "charset-normalizer==3.2.0",
        "Flask~=2.3.2",
        "Flask-HTTPAuth~=4.7",
        "future==0.18.3",
        "idna==3.4",
        "PyYAML==6.0.1",
        "requests==2.31.0",
        "statusio-python==1.3",
        "urllib3==2.0.7",
        "Werkzeug~=2.3.6",
    ],
)
