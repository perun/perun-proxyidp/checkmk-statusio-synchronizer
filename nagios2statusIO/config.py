import yaml

from yaml import SafeLoader


class Config:
    """
    Class representing configuration
    """

    def __init__(self, conf_file: str):
        """
        Constructor
        :param conf_file: path to configuration file
        """
        if not conf_file or len(conf_file.strip()) < 1:
            raise Exception("conf_file must be a non empty dict")
        config = Config.__load_config_file(conf_file)
        if not config:
            raise Exception("Could not load configuration from the file")
        basic_auth_config = config.get("basic-auth", {})
        self._ba_username = basic_auth_config.get("username", "")
        self._ba_password = basic_auth_config.get("password", "")
        status_io_config = config.get("status-io", {})
        self._api_id = status_io_config.get("api_id", "")
        self._api_key = status_io_config.get("api_key", "")
        self._status_page_id = status_io_config.get("status_page_id", "")
        self._component_mappings_enabled = "component_mappings" in status_io_config
        self._component_mappings = status_io_config.get("component_mappings", [])
        self.__validate_props()

    @property
    def get_ba_username(self):
        return self._ba_username

    @property
    def get_ba_password(self):
        return self._ba_password

    @property
    def get_api_key(self):
        return self._api_key

    @property
    def get_api_id(self):
        return self._api_id

    @property
    def get_status_page_id(self):
        return self._status_page_id

    @property
    def has_component_mappings(self):
        return self._component_mappings_enabled

    @property
    def get_component_mappings(self):
        return self._component_mappings

    @staticmethod
    def __load_config_file(file: str):
        """
        Load configuration file (YAML) and parse it
        :param file: location of the file
        :return: parsed configuration file as Dict
        """
        with open(file, "r") as stream:
            return yaml.load(stream, Loader=SafeLoader)

    def __validate_props(self):
        """
        Validate methods - should be called after setting all properties
        :return: void
        """
        if not self._ba_username or len(self._ba_username.strip()) < 1:
            raise Exception("basic-auth username has to be set")
        if not self._ba_password or len(self._ba_password.strip()) < 1:
            raise Exception("basic-auth password has to be set")
        if not self._api_id or len(self._api_id.strip()) < 1:
            raise Exception("API_ID has to be set")
        if not self._api_key or len(self._api_key.strip()) < 1:
            raise Exception("API_KEY has to be set")
        if not self._status_page_id or len(self._status_page_id.strip()) < 1:
            raise Exception("STATUS_PAGE_ID has to be set")
        if self._component_mappings_enabled and not self._component_mappings:
            raise Exception("COMPONENT_MAPPINGS enabled but not configured - has to be non-empty if specified")
