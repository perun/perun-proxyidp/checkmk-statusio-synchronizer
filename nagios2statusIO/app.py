import statusio

from nagios2statusIO.config import Config
from flask import Flask, request, jsonify, make_response, Response
from flask_httpauth import HTTPBasicAuth
from werkzeug.exceptions import Unauthorized
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.url_map.strict_slashes = False

auth = HTTPBasicAuth()

config = Config("config.yaml")

users = {config.get_ba_username: generate_password_hash(config.get_ba_password)}

def map_status(status: str) -> int | None:
    """
    Map Nagios status to Status.io code
    :param status: Nagios status STR
    :return: int code
    """
    status_map = {
        "OK": 100,  # operation
        # "MAINTENANCE": 200,  # maintenance
        "WARNING": 300,  # degraded_performance
        # "PARTIAL_DISRUPTION": 400,  # partial_service_disruption
        "CRITICAL": 500,  # service_disruption
        # "SECURITY_EVENT": 600,  # security_event
    }
    if status:
        return status_map.get(status, None)
    return None


@app.route("/status/update", methods=["POST"])
@auth.login_required
def api_handle():
    """
    Main method to process
    :return: void
    """

    body = request.get_json()

    status = body.get("status", None)
    mapped_status = map_status(status)
    message = body.get("message", None)
    component = body.get("component", None)

    if not mapped_status:
        return create_response(f"Unknown status '{status}' passed", 400)
    elif not message or len(message.strip()) < 1:
        return create_response("No message passed", 400)
    elif not component or len(component.strip()) < 1:
        return create_response("No component identifier passed", 400)

    api = statusio.Api(api_id=config.get_api_id,
                       api_key=config.get_api_key)

    component_id = None
    container_id = None

    if config.has_component_mappings:
        component_mapping = None
        for mapping in config.get_component_mappings:
            if mapping["name"] == component:
                component_mapping = mapping
        if not component_mapping:
            return create_response(f"Could not find mapping for component '{component}' in COMPONENT_MAP (config file)", 500)
        else:
            component_id = component_mapping.get("component-id", None)
            container_id = component_mapping.get("container-id", None)
    else:
        components_list = api.ComponentList(statuspage_id=config.get_status_page_id)
        if not components_list:
            return create_response("Failed to load component list via API", 500)
        for component_item in components_list["result"]:
            if "name" not in component_item:
                continue
            if component_item["name"] == component:
                if "_id" not in component_item:
                    return create_response("key _id not available in component", 500)
                component_id = component_item["_id"]
                if "containers" not in component_item:
                    return create_response("key 'containers' not available in component", 500)
                elif len(component_item["containers"]) < 1:
                    return create_response("no containers available", 500)
                elif "_id" not in component_item["containers"][0]:
                    return create_response("key _id not available in container", 500)
                container_id = component_item["containers"][0]["_id"]

    if not component_id:
        return create_response(f"Failed to resolve component_id for component '{component}'", 500)
    elif not container_id:
        return create_response(f"Failed to resolve container_id for component '{component}'", 500)

    response = api.ComponentStatusUpdate(statuspage_id=config.get_status_page_id,
                                         component=component_id,
                                         container=container_id,
                                         details=message,
                                         current_status=mapped_status)

    if not response or "status" not in response or "error" not in response["status"]:
        return create_response("Failure when calling API ", 500)
    elif response["status"]["error"] == "yes":
        return create_response(f"Calling API returned error {response['status']['message']}", 500)
    else:
        return create_response("Success", 200)


def create_response(message, code) -> Response:
    """
    Create response to request
    :return: Response object with JSON body (containing only message) correct code
    """
    app.logger.info(f"Returning response '{message}' with response code {code}")
    return make_response(jsonify(message=message), code)


@auth.verify_password
def verify_password(username, password):
    """
    Verify user credentials
    :return: username if valid credentials have been passed, exception raised otherwise
    """
    if username in users and check_password_hash(users.get(username), password):
        return username
    raise Unauthorized


def get_app(*args) -> Flask:
    """
    Get app (for UWSGI)
    :return: app
    """
    return app(*args)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=False)
