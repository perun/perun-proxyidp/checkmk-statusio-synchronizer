# CheckMK -> Status.IO synchronizer

Simple tool to synchronize the status from CheckMK to status.IO hosted page. For details of the Status.IO page see [API documentation](https://statusio.docs.apiary.io/#introduction/).

Flask app exposing API intended to be called by the Nagios/CheckMK to translate the status update to the Status.IO statuspage by calling the API.

## Configuration

All configuration options are described in the `config-template.yaml`

## Usage

Use [docker-nagios2statusio](https://gitlab.ics.muni.cz/-/ide/project/perun-proxy-aai/containers/docker-nagios2statusio/) or run directly:

```
python3 nagios2statusIO/app.py
```

## Calling the API

The API exposes a single mapping `/status/update`, which requires usage of `POST|PATCH|PUT` nad has to be authenticated using `Basic Auth`.
Expected body is in a JSON format:

```json
{
  "status": "<NAGIOS_STATUS>",
  "message": "<NAGIOS_MESSAGE>",
  "component": "<COMPONENT_IDENTIFIER>"
}
```

Where:

- `<NAGIOS_STATUS>` - one of the statuses `OK|WARNING|CRITICAL`
- `<NAGIOS_MESSAGE` - message that will be displayed as a message of status update (so should describe what has happened and why the status has changed)
- `<COMPONENT_IDENTIFIER>` - identifier of the component for which the status is being updated - needs to be agreed in advance and set in the mapping file or has to use component ID from status.IO
